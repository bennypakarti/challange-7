import React from "react";

const search = () => {
    return (
        <section class="container mb-5" id="carimobil">
            <div class="card container shadow">
                <div class="d-flex flex-column flex-md-row bd-highlight">
                    <div class="p-3 flex-fill bd-highlight">
                        <label for="form_driver" class="form-label">Tipe Driver</label>
                        <select class="form-select" id="form_driver">
                            <option selected hidden>Pilih Tipe Driver</option>
                            <option >Dengan Sopir</option>
                            <option >Tanpa Sopir (Lepas Kunci)</option>
                        </select>
                    </div>
                    <div class="p-3 flex-fill bd-highlight">
                        <label for="form_date" class="form-label">Tanggal</label>
                        <input type="date" class="form-control" id="form_date" placeholder="Pilih Tanggal"/>
                    </div>
                    <div class="p-3 flex-fill bd-highlight">
                        <label for="form_time" class="form-label">Waktu Jemput/Ambil</label>
                        <input type="time" class="form-control" id="form_time" placeholder="Pilih Waktu"/>
                    </div>
                    <div class="p-3 flex-fill bd-highlight">
                        <label for="form_pass" class="form-label">Jumlah Penumpang (Opsional)</label>
                        <input type="number" class="form-control" id="form_pass" placeholder="Jumlah Penumpang"/>
                    </div>
                    <div class="p-3 flex-fill bd-highlight" style={{margin: "auto", textAlign: "center"}}>
                        <div class="d-grid gap-2 mx-auto">
                            <button class="btn btn-sm btn-success" type="button" id="load-btn">Cari Mobil</button>
                            <button class="btn btn-sm btn-outline-primary" type="button" id="clear-btn">Reset <i class="fa-solid fa-rotate-left"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default search